package com.example.myapplication.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.myapplication.R

class homeFragment : Fragment(R.layout.fragment_home) {
    private lateinit var amountEditText : EditText
    private lateinit var sendBtn : Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        amountEditText = view.findViewById(R.id.editTextAmount)
        sendBtn = view.findViewById(R.id.buttonSend)

        val navController = Navigation.findNavController(view)

        sendBtn.setOnClickListener() {
            val amountText = amountEditText.text.toString()
            if(amountText.isEmpty()){
                return@setOnClickListener
            }
            val amount = amountText.toInt()
            val action = homeFragmentDirections.actionHomeFragmentToDashboardFragment(amount)
            navController.navigate(action)
        }
    }
}